import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import LogisticRegression

from src.utils import prepare_folds, prepare_dataset


def kfold_lr(df, model_params, num_folds, stratified):
    """Logistic Regression with KFold or Stratified KFold"""

    X_train, y_train, X_test, oof_df, sub_df = prepare_dataset(df)

    folds = prepare_folds(num_folds, stratified)

    # Create arrays and dataframes to store results
    oof_preds = np.zeros(X_train.shape[0])
    sub_preds = np.zeros(X_test.shape[0])
    result_log = []
    clf_list = []

    print("Start training")
    # Train one classifier per fold
    for n_fold, (train_idx, valid_idx) in enumerate(folds.split(X_train, y_train)):
        clf = LogisticRegression(verbose=2, n_jobs=-1, **model_params)
        clf.fit(X_train.iloc[train_idx], y_train.iloc[train_idx])
        clf_list.append(clf)

        oof_preds[valid_idx] = clf.predict_proba(X_train.iloc[valid_idx])[:, 1]
        sub_preds += clf.predict_proba(X_test)[:, 1] / folds.n_splits

        roc_auc_score_valid = roc_auc_score(y_train.iloc[valid_idx], oof_preds[valid_idx])
        roc_auc_score_train = roc_auc_score(y_train.iloc[train_idx], clf.predict_proba(X_train.iloc[train_idx])[:, 1])
        log = 'Fold {:2d} --> AUC on eval: {:.6f}  |  AUC on train: {:.6f}'.format(n_fold + 1, roc_auc_score_valid,
                                                                                   roc_auc_score_train)
        print("LR coefficients:\n", clf.coef_)
        print(log)
        result_log.append(log)

    log = 'Full AUC score {:.6f}'.format(roc_auc_score(y_train, oof_preds))
    print(log)
    result_log.append(log)

    sub_df['TARGET'] = sub_preds
    oof_df['PREDS'] = oof_preds
    return sub_df, oof_df, clf_list, result_log
