import gc

import lightgbm as lgb
import numpy as np
from sklearn.metrics import roc_auc_score

from src.utils import prepare_folds


def kfold_lightgbm(df, model_params, num_folds, stratified=False, num_boost_rounds=100000):
    """LightGBM GBDT with KFold or Stratified KFold"""
    # Divide in training/validation and test data
    train_df = df[df['TARGET'].notnull()]
    test_df = df[df['TARGET'].isnull()]
    sub_df = test_df[['SK_ID_CURR']].copy()
    oof_df = train_df[['SK_ID_CURR', 'TARGET']].copy()
    print("Starting LightGBM. Train shape: {}, test shape: {}".
          format(train_df.shape, test_df.shape))
    del df
    gc.collect()

    folds = prepare_folds(num_folds, stratified)

    # Create arrays and dataframes to store results
    oof_preds = np.zeros(train_df.shape[0])
    sub_preds = np.zeros(test_df.shape[0])
    feats = [f for f in train_df.columns if
             f not in ['TARGET', 'SK_ID_CURR', 'SK_ID_BUREAU', 'SK_ID_PREV', 'index']]

    result_log = []
    clf_list = []
    for n_fold, (train_idx, valid_idx) in enumerate(folds.split(train_df[feats], train_df['TARGET'])):
        dtrain = lgb.Dataset(data=train_df[feats].iloc[train_idx], label=train_df['TARGET'].iloc[train_idx],
                             free_raw_data=False, silent=True)
        dvalid = lgb.Dataset(data=train_df[feats].iloc[valid_idx], label=train_df['TARGET'].iloc[valid_idx],
                             free_raw_data=False, silent=True)

        clf = lgb.train(
            params=model_params,
            train_set=dtrain,
            num_boost_round=num_boost_rounds,
            valid_sets=[dtrain, dvalid],
            early_stopping_rounds=200,  # 100
            verbose_eval=100,
        )
        clf_list.append(clf)

        oof_preds[valid_idx] = clf.predict(dvalid.data)
        sub_preds += clf.predict(test_df[feats]) / folds.n_splits

        roc_auc_score_valid = roc_auc_score(dvalid.label, oof_preds[valid_idx])
        roc_auc_score_train = roc_auc_score(dtrain.label, clf.predict(dtrain.data))
        log = 'Fold {:2d} --> AUC on eval: {:.6f}  |  AUC on train: {:.6f}'.format(n_fold + 1, roc_auc_score_valid,
                                                                                   roc_auc_score_train)
        print(log)
        result_log.append(log)
        del clf, dtrain, dvalid
        gc.collect()

    log = 'Full AUC score %.6f' % roc_auc_score(train_df['TARGET'], oof_preds)
    print(log)
    result_log.append(log)

    sub_df['TARGET'] = sub_preds
    oof_df['PREDS'] = oof_preds
    return sub_df, oof_df, clf_list, result_log
