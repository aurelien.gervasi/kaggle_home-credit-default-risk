from datetime import datetime

import xgboost as xgb
import lightgbm as lgb
import numpy as np
from hyperopt import STATUS_OK, hp, Trials, fmin, tpe

from src.utils import timer


class BColours(object):
    """Color codes used by Bayesian Optimization"""
    BLUE = '\033[94m'
    CYAN = '\033[36m'
    GREEN = '\033[32m'
    MAGENTA = '\033[35m'
    RED = '\033[31m'
    ENDC = '\033[0m'


class PrintLog(object):
    """Print Log Class used by Bayesian Optimization"""

    def __init__(self, params):
        self.ymax = None
        self.xmax = None
        self.params = params
        self.ite = 1
        self.start_time = datetime.now()
        self.last_round = datetime.now()
        # sizes of parameters name and all
        self.sizes = [max(len(ps), 7) for ps in params]
        # Sorted indexes to access parameters
        self.sorti = sorted(range(len(self.params)),
                            key=self.params.__getitem__)

    def reset_timer(self):
        self.start_time = datetime.now()
        self.last_round = datetime.now()

    def print_header(self, initialization=False):
        if initialization:
            print("{}Initialization{}".format(BColours.RED,
                                              BColours.ENDC))
        else:
            print("{}Bayesian Optimization{}".format(BColours.RED,
                                                     BColours.ENDC))
        print(BColours.BLUE + "-" * (29 + sum([s + 5 for s in self.sizes])) +
              BColours.ENDC)
        print("{0:>{1}}".format("Step", 5), end=" | ")
        print("{0:>{1}}".format("Time", 6), end=" | ")
        print("{0:>{1}}".format("Value", 10), end=" | ")
        for index in self.sorti:
            print("{0:>{1}}".format(self.params[index],
                                    self.sizes[index] + 2),
                  end=" | ")
        print('')

    def print_step(self, x, y, warning=False):
        print("{:>5d}".format(self.ite), end=" | ")
        m, s = divmod((datetime.now() - self.last_round).total_seconds(), 60)
        print("{:>02d}m{:>02d}s".format(int(m), int(s)), end=" | ")

        if self.ymax is None or self.ymax < y:
            self.ymax = y
            self.xmax = x
            print("{0}{2: >10.5f}{1}".format(BColours.MAGENTA,
                                             BColours.ENDC,
                                             y),
                  end=" | ")
            for index in self.sorti:
                print("{0}{2: >{3}.{4}f}{1}".format(
                    BColours.GREEN, BColours.ENDC,
                    x[self.params[index]],
                    self.sizes[index] + 2,
                    min(self.sizes[index] - 3, 6 - 2)
                ),
                    end=" | ")
        else:
            print("{: >10.5f}".format(y), end=" | ")
            for index in self.sorti:
                print("{0: >{1}.{2}f}".format(x[self.params[index]],
                                              self.sizes[index] + 2,
                                              min(self.sizes[index] - 3, 6 - 2)),
                      end=" | ")
        if warning:
            print("{}Warning: Test point chose at "
                  "random due to repeated sample.{}".format(BColours.RED,
                                                            BColours.ENDC))
        print()
        self.last_round = datetime.now()
        self.ite += 1

    def print_summary(self):
        pass


# Class used to optimize LightGBM
class HyperOptHelperLgbm(object):
    def __init__(self, df, maximize=True, params_=None):
        self.maximize = maximize
        self.first_run = True
        self.best_score = None
        self.plog = None
        self.df = df
        self.plog = PrintLog(sorted(params_.keys()))

    def display_header(self, params_):
        print(
            '   SCORE | LEAVES | SUBSAMPLE | COLSAMPLE | MIN_SPLIT_GAIN | REG_ALPHA | REG_LAMBDA | LEAF_WEIGHT')

    def display_current_run(self, params_=None, score_=None):
        if self.first_run:
            self.plog.print_header()
            # self.display_header(params_)
            self.first_run = False

        # self.display_params(params_, score_)
        self.plog.print_step(x=params_, y=score_, warning=False)

    def is_best_score(self, score_):
        if self.best_score is None:
            self.best_score = score_
            return True
        else:
            if self.maximize:
                if score_ > self.best_score:
                    self.best_score = score_
                    return True
            else:
                if score_ < self.best_score:
                    self.best_score = score_
                    return True

    def score_params(self, params=None):
        lgb_params = {
            'objective': 'binary',
            'min_split_gain': np.power(10, params['p4_gain']),
            'subsample': params['p2_subsamp'],
            'colsample_bytree': params['p3_colsamp'],
            'reg_alpha': np.power(10, params['p5_alph']),
            'reg_lambda': np.power(10, params['p6_lamb']),
            'num_leaves': int(params['p1_leaf']),
            'verbose': -1,
            'min_child_weight': np.power(10, params['p7_weight']),
            'seed': 0,
            'boosting_type': 'gbdt',  # TODO: try goss ?
            'max_depth': -1,
            'learning_rate': 0.1
        }

        eval_hist = lgb.cv(
            params=lgb_params,
            train_set=self.df,
            num_boost_round=300,
            folds=None,
            nfold=5,
            stratified=True,
            shuffle=True,
            metrics='auc',
            early_stopping_rounds=50,
            fpreproc=None,
            verbose_eval=0,
            show_stdv=True,
            seed=0,
            callbacks=None
        )

        self.display_current_run(params, eval_hist['auc-mean'][-1])
        return {'loss': -eval_hist['auc-mean'][-1], 'status': STATUS_OK}


def run_hyperopt_lgbm(df):
    features = [f for f in df.columns
                if f not in ['TARGET', 'SK_ID_CURR', 'SK_ID_BUREAU', 'SK_ID_PREV', 'index']]
    dtrain = lgb.Dataset(
        data=df.loc[df['TARGET'].notnull(), features],
        label=df.loc[df['TARGET'].notnull(), 'TARGET'],
        free_raw_data=False
    )
    del df

    with timer("Run Hyperopt onLightGBM"):
        param_space = {
            'p1_leaf': hp.uniform('p1_leaf', 5, 80),
            'p2_subsamp': hp.uniform('p2_subsamp', 0.6, 0.95),
            'p3_colsamp': hp.uniform('p3_colsamp', 0.6, 0.95),
            'p7_weight': hp.uniform('min_child_weight', -5, 2),
            'p4_gain': hp.uniform('p4_gain', -5, 2),
            'p5_alph': hp.uniform('reg_alph', -5, 2),
            'p6_lamb': hp.uniform('reg_lamb', -5, 2)
        }
        np.random.seed(100)
        trials = Trials()
        best = fmin(
            fn=HyperOptHelperLgbm(df=dtrain, maximize=True, params_=param_space).score_params,
            space=param_space,
            algo=tpe.suggest,
            max_evals=150,
            trials=trials,
            return_argmin=True
        )


def run_hyperopt_xgb(df):
    train_df = df[df['TARGET'].notnull()]
    feats = [f for f in train_df.columns if
             f not in ['TARGET', 'SK_ID_CURR', 'SK_ID_BUREAU', 'SK_ID_PREV', 'index']]
    dtrain = xgb.DMatrix(data=train_df[feats].values,
                         label=train_df["TARGET"].values,
                         missing=np.nan, silent=True)

    param_space = {
        'max_depth': hp.quniform("max_depth", 3, 10, 1),
        'eta': hp.loguniform("eta", -2, 1),
        'min_child_weight': hp.quniform('min_child_weight', 1, 50, 5),
        'subsample': hp.quniform('subsample', 0.6, 1, 0.1),
        'colsample_bytree': hp.quniform('colsample_bytree', 0.6, 1, 0.1),
        'min_samples_leaf': hp.quniform('min_samples_leaf', 1, 10, 1),
        'min_samples_split': hp.quniform('min_samples_split', 2, 20, 1),
        'criterion': hp.choice('criterion', ('gini', 'entropy')),
        'class_weight': hp.choice('class_weight', ('balanced', 'balanced_subsample', None)),
    }

    trials = Trials()
    with timer("Run Hyperopt on XGBoost"):
        best = fmin(fn=HyperOptHelperXgb(df=dtrain, params_=param_space).score_params,
                    space=param_space,
                    algo=tpe.suggest,
                    max_evals=30,
                    trials=trials,
                    return_argmin=True)

    print(best)


class HyperOptHelperXgb(object):
    def __init__(self, df, params_=None):
        self.df = df

    def score_params(self, params=None):
        xgb_params = {
            'n_estimators': 10000,
            'learning_rate': 0.2,
            'silent': 1,
            'max_depth': int(params['max_depth']),
            'eta': float(params['eta']),
            'min_child_weight': int(params['min_child_weight']),
            'subsample': float(params['subsample']),
            'colsample_bytree': float(params['colsample_bytree']),
            'min_samples_leaf': int(params['min_samples_leaf']),
            'min_samples_split': int(params['min_samples_split']),
            'criterion': params['criterion'],
            'class_weight': params['colsample_bytree'],
        }

        eval_hist = xgb.cv(
            params=xgb_params,
            dtrain=self.df,
            num_boost_round=15,
            folds=None,
            nfold=5,
            stratified=False,
            shuffle=True,
            metrics='auc',
            early_stopping_rounds=2,
            verbose_eval=5,
            show_stdv=True,
            seed=0,
            callbacks=None,
        )

        #self.display_current_run(params, eval_hist['auc-mean'][-1])
        print(xgb_params)
        auc_train = eval_hist['train-auc-mean'].values[-1]
        auc_valid = eval_hist['test-auc-mean'].values[-1]
        print("AUC on train: {:.6f}   |   AUC on valid: {:.6f}".format(auc_train, auc_valid))
        return {'loss': -auc_valid, 'status': STATUS_OK}