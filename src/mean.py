import gc

from sklearn.metrics import roc_auc_score


def meta_mean(df, model_params):
    # Divide in training/validation and test data
    train_df = df[df['TARGET'].notnull()]
    test_df = df[df['TARGET'].isnull()]
    print("Starting Averaging. Train shape: {}, test shape: {}".format(train_df.shape,
                                                                       test_df.shape))
    del df
    gc.collect()

    train_df['PREDS'] = 0.5 * train_df['PREDS_lgbm_1'] + 0.5 * train_df['PREDS_lgbm_2']
    test_df['TARGET'] = 0.5 * test_df['PREDS_lgbm_1'] + 0.5 * test_df['PREDS_lgbm_2']
    sub_df = test_df[['SK_ID_CURR', 'TARGET']]
    oof_df = train_df[['SK_ID_CURR', 'PREDS']]

    roc_auc_train = roc_auc_score(train_df['TARGET'], train_df['PREDS'])
    log = 'Full AUC score {:.6f}'.format(roc_auc_train)
    print(log)
    clf_list = None
    return sub_df, oof_df, clf_list, [log]
