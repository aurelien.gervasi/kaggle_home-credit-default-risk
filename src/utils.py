import gc
import random
import time
from contextlib import contextmanager

import numpy as np

import pandas as pd
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.preprocessing import Imputer, StandardScaler

random.seed(42)

@contextmanager
def timer(title):
    print(title)
    t0 = time.time()
    yield
    print("{} - done in {:.0f}s".format(title, time.time() - t0))


def load_csv_to_df(csv_path, num_rows=None):
    if num_rows:
        n = sum(1 for line in open(csv_path)) - 1  # number of records in file (excludes header)
        s = num_rows  # desired sample size
        skip = sorted(random.sample(range(1, n + 1), n - s))  # the 0-indexed header will not be included in the skip list
        df = pd.read_csv(csv_path, skiprows=skip)
    else:
        df = pd.read_csv(csv_path)
    print("Loaded csv: ", df.shape)
    return df


def prepare_folds(num_folds, stratified):
    if stratified:
        folds = StratifiedKFold(n_splits=num_folds, shuffle=True, random_state=1001)
    else:
        folds = KFold(n_splits=num_folds, shuffle=True, random_state=1001)
    return folds


def prepare_dataset(df, standardize=False):
    # Divide in training/validation and test data
    train_df = df[df['TARGET'].notnull()]
    test_df = df[df['TARGET'].isnull()]
    del df
    gc.collect()

    # Instanciate results dataframes
    sub_df = test_df[['SK_ID_CURR']].copy()
    oof_df = train_df[['SK_ID_CURR', 'TARGET']].copy()
    train_df = train_df.set_index('SK_ID_CURR')
    test_df = test_df.set_index('SK_ID_CURR')

    # Split target from features
    y_train = train_df['TARGET']
    X_train_raw = train_df.drop(['TARGET', 'index'], axis=1)
    X_test_raw = test_df.drop(['TARGET', 'index'], axis=1)
    print("Starting stacking with CV. Train shape: {}, test shape: {}".format(train_df.shape, test_df.shape))
    del train_df, test_df
    gc.collect()

    # Fill nan
    fill_NaN = Imputer(missing_values=np.nan, strategy='median')
    X_train_imp = pd.DataFrame(fill_NaN.fit_transform(X_train_raw), columns=X_train_raw.columns,
                               index=X_train_raw.index)
    X_test_imp = pd.DataFrame(fill_NaN.transform(X_test_raw), columns=X_test_raw.columns, index=X_test_raw.index)
    del X_train_raw
    gc.collect()

    # Standardize
    if standardize:
        scaler = StandardScaler()
        X_train_stand = pd.DataFrame(scaler.fit_transform(X_train_imp), columns=X_train_imp.columns,
                                     index=X_train_imp.index)
        X_test_stand = pd.DataFrame(scaler.transform(X_test_imp), columns=X_test_imp.columns, index=X_test_imp.index)
        del X_train_imp
        gc.collect()
        return X_train_stand, y_train, X_test_stand, oof_df, sub_df

    X_train = X_train_imp
    X_test = X_test_imp
    return X_train, y_train, X_test, oof_df, sub_df


def reduce_mem_usage(df):
    start_mem_usg = df.memory_usage().sum() / 1024 ** 2
    print("Memory usage of properties dataframe is :", start_mem_usg, " MB")
    NAlist = []  # Keeps track of columns that have missing values filled in.
    cols_to_optimize = [col for col in df.columns if col not in ['TARGET',]]
    for col in cols_to_optimize:
        if df[col].dtype != object:  # Exclude strings

            # Print current column type
            print("******************************")
            print("Column: ", col)
            print("dtype before: ", df[col].dtype)

            # make variables for Int, max and min
            IsInt = False
            mx = df[col].max()
            mn = df[col].min()

            # Integer does not support NA, therefore, NA needs to be filled
            if not np.isfinite(df[col]).all():
                NAlist.append(col)
                df[col].fillna(df[col].median(), inplace=True)

                # test if column can be converted to an integer
            asint = df[col].fillna(0).astype(np.int64)
            result = (df[col] - asint)
            result = result.sum()
            if result > -0.01 and result < 0.01:
                IsInt = True

            # Make Integer/unsigned Integer datatypes
            if IsInt:
                if mn >= 0:
                    if mx < 255:
                        df[col] = df[col].astype(np.uint8)
                    elif mx < 65535:
                        df[col] = df[col].astype(np.uint16)
                    elif mx < 4294967295:
                        df[col] = df[col].astype(np.uint32)
                    else:
                        df[col] = df[col].astype(np.uint64)
                else:
                    if mn > np.iinfo(np.int8).min and mx < np.iinfo(np.int8).max:
                        df[col] = df[col].astype(np.int8)
                    elif mn > np.iinfo(np.int16).min and mx < np.iinfo(np.int16).max:
                        df[col] = df[col].astype(np.int16)
                    elif mn > np.iinfo(np.int32).min and mx < np.iinfo(np.int32).max:
                        df[col] = df[col].astype(np.int32)
                    elif mn > np.iinfo(np.int64).min and mx < np.iinfo(np.int64).max:
                        df[col] = df[col].astype(np.int64)

                        # Make float datatypes 32 bit
            else:
                df[col] = df[col].astype(np.float32)

            # Print new column type
            print("dtype after: ", df[col].dtype)
            print("******************************")

    # Print final result
    print("___MEMORY USAGE AFTER COMPLETION:___")
    mem_usg = df.memory_usage().sum() / 1024 ** 2
    print("Memory usage is: ", mem_usg, " MB")
    print("This is ", 100 * mem_usg / start_mem_usg, "% of the initial size")
    return df