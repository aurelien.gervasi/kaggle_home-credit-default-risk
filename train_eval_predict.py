import argparse
import gc
import json
import os
import pickle

import numpy as np
import pandas as pd

from src.lightgbm import kfold_lightgbm
from src.mean import meta_mean
from src.mlp import kfold_mlp
from src.stacking import kfold_lr
from src.utils import timer, load_csv_to_df, reduce_mem_usage
from src.hyperopt import run_hyperopt_lgbm, run_hyperopt_xgb
from src.xgboost import kfold_xgb

submission_data_dir = "data/submission"


def build_dataset_for_metamodel(model_dir, base_model_list, df_base=None):

    # Merge prediction from base model (on training set)
    df_list = []
    for model_name in base_model_list:
        prediction_filepath = os.path.join(os.path.dirname(model_dir), model_name, 'preds_oof.csv')
        df = load_csv_to_df(prediction_filepath)
        if df_base is not None:
            df = df[df['SK_ID_CURR'].isin(df_base['SK_ID_CURR'])]  # Select same rows as base feature dataframe
        df.rename(columns={'PREDS': 'PREDS_{}'.format(model_name)}, inplace=True)
        df.set_index('SK_ID_CURR', inplace=True)
        df_list.append(df)
    df_concat_train = df_list[0][['TARGET']].copy()
    for df, model_name in zip(df_list, base_model_list):
        df_concat_train = df_concat_train.join(df['PREDS_{}'.format(model_name)])
    df_concat_train.reset_index(inplace=True)

    # Merge prediction from base model (on test set)
    df_list = []
    for model_name in base_model_list:
        prediction_filepath = os.path.join(os.path.dirname(model_dir), model_name, 'submision.csv')
        df = load_csv_to_df(prediction_filepath)
        if df_base is not None:
            df = df[df['SK_ID_CURR'].isin(df_base['SK_ID_CURR'])]  # Select same rows as base feature dataframe
        df.rename(columns={'TARGET': 'PREDS_{}'.format(model_name)}, inplace=True)
        df.set_index('SK_ID_CURR', inplace=True)
        df_list.append(df)
    df_concat_test = df_list[0].copy()
    for df, model_name in zip(df_list[1:], base_model_list[1:]):
        df_concat_test = df_concat_test.join(df['PREDS_{}'.format(model_name)])
    df_concat_test.reset_index(inplace=True)
    df_concat_test['TARGET'] = np.nan

    # Merge meta features from train and test
    df_stacking = pd.concat([df_concat_train, df_concat_test], axis=0)
    df_stacking.reset_index(inplace=True)

    # Merge meta feature with base feature if needed
    if df_base is not None:
        cols_to_merge = [col for col in df_stacking.columns if 'PREDS_' in col]
        df_concat = pd.concat([df_base, df_stacking[cols_to_merge]], axis=1)
        return df_concat
    else:
        return df_stacking


def train_evaluate_save_model(src_csv, model_dir, num_boost_rounds, debug):
    with open(os.path.join(model_dir, 'model_config.json'), 'r') as f:
        model_config = json.load(f)
    model_type = model_config['model_type']
    model_params = model_config['model_params']

    with timer("Load csv to df"):
        num_rows = 10000 if args.debug else None
        df = load_csv_to_df(src_csv, num_rows)
        df = reduce_mem_usage(df)

    if model_type == 'lgbm':
        with timer("Train model with kfold"):
            sub_df, oof_df, models, result_log = kfold_lightgbm(df, model_params, num_folds=5, stratified=False,
                                                                num_boost_rounds=num_boost_rounds)
    if model_type == 'xgb':
        with timer("Train model with kfold"):
            sub_df, oof_df, models, result_log = kfold_xgb(df, model_params, num_folds=5, stratified=False,
                                                           num_boost_rounds=num_boost_rounds)
    if model_type == 'mlp':
        with timer("Train model with kfold"):
            sub_df, oof_df, models, result_log = kfold_mlp(df, model_params, num_folds=5, stratified=False)
    elif model_type == 'meta_lr':
        base_model_list = model_config['base_model_list']
        with timer("Build dataset for metamodel"):
            df_meta = build_dataset_for_metamodel(model_dir, base_model_list)
            del df
            gc.collect()

        with timer("Train LR meta-model with kfold"):
            sub_df, oof_df, models, result_log = kfold_lr(df_meta, model_params, num_folds=5, stratified=False)

    elif model_type == 'meta_mean':
        base_model_list = model_config['base_model_list']
        with timer("Build dataset for metamodel"):
            df = build_dataset_for_metamodel(model_dir, base_model_list)

            with timer("Build averaging meta-model with kfold"):
                    sub_df, oof_df, models, result_log = meta_mean(df, model_params)

    with timer("Save predictions and result logs."):
        # Write submission file and result log
        submission_filepath = os.path.join(model_dir, 'submision.csv')
        oof_filepath = os.path.join(model_dir, 'preds_oof.csv')
        result_log_filepath = os.path.join(model_dir, 'training_log.txt')
        sub_df[['SK_ID_CURR', 'TARGET']].to_csv(submission_filepath, index=False)
        oof_df.to_csv(oof_filepath, index=False)
        with open(result_log_filepath, 'w') as f:
            f.write('\n'.join(result_log))

        # Save models as pickle
        model_filepath = os.path.join(model_dir, 'models.pickle')
        with open(model_filepath, 'wb') as f:
            pickle.dump(models, f)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", default=False, type=bool)
    parser.add_argument("--num_boost_rounds", default=1000000, type=int)
    parser.add_argument("--src_csv", default='data/feature_selection/df.csv')
    parser.add_argument("--hyperopt", default=False)
    parser.add_argument("--model_dir")
    args = parser.parse_args()


    if args.hyperopt:
        with timer("Load csv to df"):
            num_rows = 10000 if args.debug else None
            df = load_csv_to_df(args.src_csv, num_rows)
        if args.hyperopt == 'lgbm':
            with timer("Hyperopt"):
                run_hyperopt_lgbm(df)
        if args.hyperopt == 'xgb':
            with timer("Hyperopt"):
                run_hyperopt_xgb(df)

    else:
        with timer("Train, Evaluate, Predict"):
            train_evaluate_save_model(args.src_csv, args.model_dir,
                                      num_boost_rounds=args.num_boost_rounds,
                                      debug=args.debug)
