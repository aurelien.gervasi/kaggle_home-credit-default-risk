import argparse
import gc
import os

import numpy as np
import pandas as pd

from src.utils import timer

raw_data_dir = "data/raw"
processed_data_dir = "data/processed"


def one_hot_encoder(df, nan_as_category=True):
    """One-hot encoding for categorical columns with get_dummies"""
    original_columns = list(df.columns)
    categorical_columns = [col for col in df.columns if df[col].dtype == 'object']
    df = pd.get_dummies(df, columns=categorical_columns, dummy_na=nan_as_category)
    new_columns = [c for c in df.columns if c not in original_columns]
    return df, new_columns


def application_train_test(num_rows=None, nan_as_category=False):
    """Preprocess application_train.csv and application_test.csv"""
    # Read data and merge
    df = pd.read_csv(os.path.join(raw_data_dir, 'application_train.csv'), nrows=num_rows)
    test_df = pd.read_csv(os.path.join(raw_data_dir, 'application_test.csv'), nrows=num_rows)
    print("Train samples: {}, test samples: {}".format(len(df), len(test_df)))
    df = df.append(test_df, sort=False).reset_index()

    # Optional: Remove 4 applications with XNA CODE_GENDER (train set)
    df = df[df['CODE_GENDER'] != 'XNA']

    docs = [colname for colname in df.columns if 'FLAG_DOC' in colname]
    live = [colname for colname in df.columns if
            ('FLAG_' in colname) & ('FLAG_DOC' not in colname) & ('_FLAG_' not in colname)]

    # NaN values for DAYS_EMPLOYED: 365.243 -> nan
    df.loc[df['DAYS_EMPLOYED'] == 365243, 'ANOM'] = 1
    df['DAYS_EMPLOYED'].replace(365243, np.nan, inplace=True)

    inc_by_org = \
        df[['AMT_INCOME_TOTAL', 'ORGANIZATION_TYPE']].groupby('ORGANIZATION_TYPE').median()[
            'AMT_INCOME_TOTAL']

    df['NEW_CREDIT_TO_ANNUITY_RATIO'] = df['AMT_CREDIT'] / (df['AMT_ANNUITY'] + .01)
    df['NEW_CREDIT_TO_GOODS_RATIO'] = df['AMT_CREDIT'] / (df['AMT_GOODS_PRICE'] + .01)
    df['NEW_DOC_IND_AVG'] = df[docs].mean(axis=1)
    df['NEW_DOC_IND_STD'] = df[docs].std(axis=1)
    df['NEW_DOC_IND_KURT'] = df[docs].kurtosis(axis=1)
    df['NEW_LIVE_IND_SUM'] = df[live].sum(axis=1)
    df['NEW_LIVE_IND_STD'] = df[live].std(axis=1)
    df['NEW_LIVE_IND_KURT'] = df[live].kurtosis(axis=1)
    df['NEW_INC_PER_CHLD'] = df['AMT_INCOME_TOTAL'] / (1 + df['CNT_CHILDREN'])
    df['NEW_INC_BY_ORG'] = df['ORGANIZATION_TYPE'].map(inc_by_org)
    df['NEW_EMPLOY_TO_BIRTH_RATIO'] = df['DAYS_EMPLOYED'] / (df['DAYS_BIRTH'] + .01)
    df['NEW_ANNUITY_TO_INCOME_RATIO'] = df['AMT_ANNUITY'] / (1 + df['AMT_INCOME_TOTAL'])
    df['NEW_SOURCES_PROD'] = df['EXT_SOURCE_1'] * df['EXT_SOURCE_2'] * df['EXT_SOURCE_3']
    df['NEW_EXT_SOURCES_MEAN'] = df[['EXT_SOURCE_1', 'EXT_SOURCE_2', 'EXT_SOURCE_3']].mean(axis=1)
    df['NEW_SCORES_STD'] = df[['EXT_SOURCE_1', 'EXT_SOURCE_2', 'EXT_SOURCE_3']].std(axis=1)
    df['NEW_SCORES_STD'] = df['NEW_SCORES_STD'].fillna(df['NEW_SCORES_STD'].mean())
    df['NEW_CAR_TO_BIRTH_RATIO'] = df['OWN_CAR_AGE'] / (df['DAYS_BIRTH'] + .01)
    df['NEW_CAR_TO_EMPLOY_RATIO'] = df['OWN_CAR_AGE'] / (df['DAYS_EMPLOYED'] + .01)
    df['NEW_PHONE_TO_BIRTH_RATIO'] = df['DAYS_LAST_PHONE_CHANGE'] / (df['DAYS_BIRTH'] + .01)
    df['NEW_PHONE_TO_EMPLOY_RATIO'] = df['DAYS_LAST_PHONE_CHANGE'] / (df['DAYS_EMPLOYED'] + .01)
    df['NEW_CREDIT_TO_INCOME_RATIO'] = df['AMT_CREDIT'] / (df['AMT_INCOME_TOTAL'] + .01)

    # Categorical features with Binary encode (0 or 1; two categories)
    for bin_feature in ['CODE_GENDER', 'FLAG_OWN_CAR', 'FLAG_OWN_REALTY']:
        df[bin_feature], uniques = pd.factorize(df[bin_feature])
    # Categorical features with One-Hot encode
    df, cat_cols = one_hot_encoder(df, nan_as_category)

    del test_df
    gc.collect()
    return df


def bureau_and_balance(num_rows=None, nan_as_category=True):
    """Preprocess bureau.csv and bureau_balance.csv"""
    bureau = pd.read_csv(os.path.join(raw_data_dir, 'bureau.csv'), nrows=num_rows)
    bb = pd.read_csv(os.path.join(raw_data_dir, 'bureau_balance.csv'), nrows=num_rows)
    bb, bb_cat = one_hot_encoder(bb, nan_as_category)
    bureau, bureau_cat = one_hot_encoder(bureau, nan_as_category)

    # Aggregate bureau balance for each bureau ID
    bb_agg, bb_cat = aggregate_bb(bb, bb_cat)
    bureau = bureau.join(bb_agg, how='left', on='SK_ID_BUREAU')
    bureau.drop(['SK_ID_BUREAU'], axis=1, inplace=True)
    del bb, bb_agg
    gc.collect()

    # Aggregate bureau and bureau balance feature for each application ID
    bureau_agg = aggregate_bureau_and_bureau_balance(bb_cat, bureau, bureau_cat)
    return bureau_agg


def aggregate_bureau_and_bureau_balance(bb_cat, bureau, bureau_cat):
    # Define whiche columns have to use weighted average
    cols_to_apply_weights = bureau_cat
    cols_to_apply_weights.extend([
        'DAYS_CREDIT', 'DAYS_CREDIT_ENDDATE', 'DAYS_CREDIT_UPDATE', 'CREDIT_DAY_OVERDUE',
        'AMT_CREDIT_MAX_OVERDUE', 'AMT_CREDIT_SUM', 'AMT_CREDIT_SUM_DEBT',
        'AMT_CREDIT_SUM_OVERDUE', 'AMT_CREDIT_SUM_LIMIT', 'AMT_ANNUITY', 'MONTHS_BALANCE_SIZE'])

    bureau, weighted_cols = apply_freshness_coefficient(bureau, cols_to_apply_weights,
                                                        'DAYS_CREDIT_UPDATE', 'SK_ID_CURR')
    # Bureau and bureau_balance numeric features
    numerical_agg = {
        'DAYS_CREDIT': ['min', 'max', 'mean', 'var'],
        'DAYS_CREDIT_ENDDATE': ['min', 'max', 'mean'],
        'DAYS_CREDIT_UPDATE': ['mean'],
        'CREDIT_DAY_OVERDUE': ['max', 'mean'],
        'AMT_CREDIT_MAX_OVERDUE': ['mean'],
        'AMT_CREDIT_SUM': ['max', 'mean', 'sum'],
        'AMT_CREDIT_SUM_DEBT': ['max', 'mean', 'sum'],
        'AMT_CREDIT_SUM_OVERDUE': ['mean'],
        'AMT_CREDIT_SUM_LIMIT': ['mean', 'sum'],
        'AMT_ANNUITY': ['max', 'mean'],
        'CNT_CREDIT_PROLONG': ['sum'],
        'MONTHS_BALANCE_MIN': ['min'],
        'MONTHS_BALANCE_MAX': ['max'],
        'MONTHS_BALANCE_SIZE': ['mean', 'sum']
    }
    # Bureau and bureau_balance categorical features
    categorical_agg = {}
    weighted_agg = {}
    for cat in bureau_cat: categorical_agg[cat] = ['mean']
    for cat in bb_cat: categorical_agg[cat] = ['mean']
    for colname in weighted_cols: weighted_agg[colname] = ['sum']

    bureau_agg = bureau.groupby('SK_ID_CURR').agg({**numerical_agg, **categorical_agg,
                                                   **weighted_agg})
    bureau_agg.columns = pd.Index(
        ['BURO_' + e[0] + "_" + e[1].upper() for e in bureau_agg.columns.tolist()])
    # Bureau: Active credits - using only numerical aggregations
    active = bureau[bureau['CREDIT_ACTIVE_Active'] == 1]
    active_agg = active.groupby('SK_ID_CURR').agg(numerical_agg)
    cols_credit_active = active_agg.columns.tolist()
    active_agg.columns = pd.Index(
        ['ACTIVE_' + e[0] + "_" + e[1].upper() for e in active_agg.columns.tolist()])
    bureau_agg = bureau_agg.join(active_agg, how='left', on='SK_ID_CURR')
    del active, active_agg
    gc.collect()
    # Bureau: Closed credits - using only numerical aggregations
    closed = bureau[bureau['CREDIT_ACTIVE_Closed'] == 1]
    closed_agg = closed.groupby('SK_ID_CURR').agg(numerical_agg)
    closed_agg.columns = pd.Index(
        ['CLOSED_' + e[0] + "_" + e[1].upper() for e in closed_agg.columns.tolist()])
    bureau_agg = bureau_agg.join(closed_agg, how='left', on='SK_ID_CURR')
    for e in cols_credit_active:
        col_name = "{}_{}".format(e[0], e[1].upper())
        new_col_name = 'NEW_RATIO_BURO_{}'.format(col_name)
        bureau_agg[new_col_name] = bureau_agg['ACTIVE_' + col_name] / (bureau_agg['CLOSED_' + col_name] + 0.01)
    del closed, closed_agg, bureau
    gc.collect()
    return bureau_agg


def aggregate_bb(bb, bb_cat):
    """Bureau balance: Perform aggregations and merge with bureau.csv"""
    bb, weighted_cols = apply_freshness_coefficient(bb, bb_cat,
                                                    'MONTHS_BALANCE', 'SK_ID_BUREAU')

    # Aggregation
    bb_aggregations = {'MONTHS_BALANCE': ['min', 'max', 'size']}
    for col in bb_cat: bb_aggregations[col] = ['mean']
    weighted_agg = {col: ['sum'] for col in weighted_cols}

    bb_agg = bb.groupby('SK_ID_BUREAU').agg({**bb_aggregations, **weighted_agg})
    bb_agg.columns = pd.Index([e[0] + "_" + e[1].upper() for e in bb_agg.columns.tolist()])
    weighted_cols = [col + '_SUM' for col in weighted_cols]
    bb_cat = [col + '_MEAN' for col in bb_cat]
    bb_cat.extend(weighted_cols)

    return bb_agg, bb_cat


def previous_applications(num_rows=None, nan_as_category=True):
    """Preprocess previous_applications.csv"""
    prev = pd.read_csv(os.path.join(raw_data_dir, 'previous_application.csv'), nrows=num_rows)
    prev, cat_cols = one_hot_encoder(prev, nan_as_category=True)

    # Handle anomalies
    # Days 365.243 values -> nan
    for col in ['DAYS_FIRST_DRAWING', 'DAYS_FIRST_DUE', 'DAYS_LAST_DUE_1ST_VERSION', 'DAYS_LAST_DUE', 'DAYS_TERMINATION']:
        prev.loc[prev[col] == 365243, 'PREV_ANOM'] = 1
        prev[col].replace(365243, np.nan, inplace=True)

    # Add features
    # Add feature: value ask / value received percentage
    prev['APP_CREDIT_PERC'] = prev['AMT_APPLICATION'] / (prev['AMT_CREDIT'] + .01)

    # Aggregate features
    prev_agg = aggregate_previous_applications(cat_cols, prev)
    return prev_agg


def aggregate_previous_applications(cat_cols, prev):
    # Define whiche columns have to use weighted average
    cols_to_apply_weights = cat_cols
    cols_to_apply_weights.extend([
        'AMT_ANNUITY', 'AMT_APPLICATION', 'AMT_CREDIT', 'APP_CREDIT_PERC',
        'AMT_DOWN_PAYMENT', 'AMT_GOODS_PRICE',
        'HOUR_APPR_PROCESS_START', 'RATE_DOWN_PAYMENT', 'DAYS_DECISION', 'CNT_PAYMENT'])
    prev, weighted_cols = apply_freshness_coefficient(prev, cat_cols,
                                                      'DAYS_DECISION', 'SK_ID_CURR')

    # Previous applications numeric features
    numerical_agg = {
        'AMT_ANNUITY': ['min', 'max', 'mean'],
        'AMT_APPLICATION': ['min', 'max', 'mean'],
        'AMT_CREDIT': ['min', 'max', 'mean'],
        'APP_CREDIT_PERC': ['min', 'max', 'mean', 'var'],
        'AMT_DOWN_PAYMENT': ['min', 'max', 'mean'],
        'AMT_GOODS_PRICE': ['min', 'max', 'mean'],
        'HOUR_APPR_PROCESS_START': ['min', 'max', 'mean'],
        'RATE_DOWN_PAYMENT': ['min', 'max', 'mean'],
        'DAYS_DECISION': ['min', 'max', 'mean'],
        'CNT_PAYMENT': ['mean', 'sum'],
    }
    # Previous applications categorical features
    categorical_agg = {cat: ['mean'] for cat in cat_cols}
    weighted_agg = {col: ['sum'] for col in weighted_cols}

    prev_agg = prev.groupby('SK_ID_CURR').agg({**numerical_agg, **categorical_agg, **weighted_agg})
    prev_agg.columns = pd.Index(
        ['{}_{}'.format(e[0], e[1].upper()) for e in prev_agg.columns.tolist()])
    weighted_cols = [col + '_SUM' for col in weighted_cols]
    cat_cols = [col + '_MEAN' for col in cat_cols]
    prev_agg.columns = ['PREV_' + col for col in prev_agg.columns]

    # Previous Applications: Approved Applications - only numerical features
    approved = prev[prev['NAME_CONTRACT_STATUS_Approved'] == 1]
    approved_agg = approved.groupby('SK_ID_CURR').agg(numerical_agg)
    cols = approved_agg.columns.tolist()
    approved_agg.columns = pd.Index(
        ['APPROVED_{}_{}'.format(e[0], e[1].upper()) for e in approved_agg.columns.tolist()])
    prev_agg = prev_agg.join(approved_agg, how='left', on='SK_ID_CURR')
    # Previous Applications: Refused Applications - only numerical features
    refused = prev[prev['NAME_CONTRACT_STATUS_Refused'] == 1]
    refused_agg = refused.groupby('SK_ID_CURR').agg(numerical_agg)
    refused_agg.columns = pd.Index(
        ['REFUSED_{}_{}'.format(e[0], e[1].upper()) for e in refused_agg.columns.tolist()])
    prev_agg = prev_agg.join(refused_agg, how='left', on='SK_ID_CURR')
    del refused, refused_agg, approved, approved_agg, prev
    for e in cols:
        col_name = "{}_{}".format(e[0], e[1].upper())
        new_col_name = 'NEW_RATIO_PREV_{}'.format(col_name)
        prev_agg[new_col_name] = prev_agg['APPROVED_' + col_name] / (prev_agg['REFUSED_' + col_name] + 0.01)
    gc.collect()
    return prev_agg


def apply_freshness_coefficient(df, cat_cols, freshness_col, agg_key, time_period=12):
    """Apply freshness coefficient to categorical values"""
    df['COEFF_TIME_AGGREGATION'] = 1 / (np.abs(df[freshness_col] / time_period) + 1)
    coeff_time_sum_per_agg_key = df.groupby(agg_key)['COEFF_TIME_AGGREGATION'].sum()
    weighted_cols = []
    for col in cat_cols:
        df[col + '_w'] = df[col] * df['COEFF_TIME_AGGREGATION'] / df[agg_key].map(
            coeff_time_sum_per_agg_key)
        weighted_cols.append(col + '_w')
    df.drop(['COEFF_TIME_AGGREGATION'], axis=1, inplace=True)
    return df, weighted_cols


def pos_cash(num_rows=None, nan_as_category=True):
    """Preprocess POS_CASH_balance.csv"""
    pos = pd.read_csv(os.path.join(raw_data_dir, 'POS_CASH_balance.csv'), nrows=num_rows)
    pos, cat_cols = one_hot_encoder(pos, nan_as_category=True)
    # Aggregate and add features
    pos_agg = aggregate_posh_cash(pos, cat_cols)
    del pos
    gc.collect()
    return pos_agg


def aggregate_posh_cash(pos, cat_cols):
    pos, weighted_cols = apply_freshness_coefficient(pos, cat_cols,
                                                     'MONTHS_BALANCE', 'SK_ID_CURR')

    aggregations = {
        'MONTHS_BALANCE': ['max', 'mean', 'size'],
        'SK_DPD': ['max', 'mean'],
        'SK_DPD_DEF': ['max', 'mean']
    }
    for cat in cat_cols:
        aggregations[cat] = ['mean']
    weighted_agg = {col: ['sum'] for col in weighted_cols}

    pos_agg = pos.groupby('SK_ID_CURR').agg({**aggregations, **weighted_agg})
    pos_agg.columns = pd.Index(
        ['{}_{}'.format(e[0], e[1].upper()) for e in pos_agg.columns.tolist()])
    weighted_cols = [col + '_SUM' for col in weighted_cols]
    cat_cols = [col + '_MEAN' for col in cat_cols]

    # Add prefix to all columns
    pos_agg.columns = ['POS_' + col for col in pos_agg.columns]

    # Count pos cash accounts
    pos_agg['POS_COUNT'] = pos.groupby('SK_ID_CURR').size()
    return pos_agg


def installments_payments(num_rows=None, nan_as_category=True):
    """Preprocess installments_payments.csv"""
    ins = pd.read_csv(os.path.join(raw_data_dir, 'installments_payments.csv'), nrows=num_rows)
    ins, cat_cols = one_hot_encoder(ins, nan_as_category=True)

    # Add features
    # Percentage and difference paid in each installment (amount paid and installment value)
    ins['PAYMENT_PERC'] = ins['AMT_PAYMENT'] / (ins['AMT_INSTALMENT'] + 0.01)
    ins['PAYMENT_DIFF'] = ins['AMT_INSTALMENT'] - ins['AMT_PAYMENT']
    # Days past due and days before due (no negative values)
    ins['DPD'] = ins['DAYS_ENTRY_PAYMENT'] - ins['DAYS_INSTALMENT']
    ins['DBD'] = ins['DAYS_INSTALMENT'] - ins['DAYS_ENTRY_PAYMENT']
    ins['DPD'] = ins['DPD'].apply(lambda x: x if x > 0 else 0)
    ins['DBD'] = ins['DBD'].apply(lambda x: x if x > 0 else 0)

    # Features: Perform aggregations
    ins_agg = aggregate_installments(ins, cat_cols)
    return ins_agg


def aggregate_installments(ins, cat_cols):
    cols_to_apply_weights = cat_cols
    cols_to_apply_weights.extend([
        'DPD', 'DBD', 'PAYMENT_PERC', 'PAYMENT_DIFF', 'AMT_INSTALMENT', 'AMT_PAYMENT',
        'DAYS_ENTRY_PAYMENT', ])
    ins, weighted_cols = apply_freshness_coefficient(ins, cols_to_apply_weights, 'DAYS_INSTALMENT',
                                                     'SK_ID_CURR', time_period=30)

    non_weighted_aggregations = {
        'NUM_INSTALMENT_VERSION': ['nunique'],
        'DPD': ['max', 'mean', 'sum'],
        'DBD': ['max', 'mean', 'sum'],
        'PAYMENT_PERC': ['max', 'mean', 'sum', 'var'],
        'PAYMENT_DIFF': ['max', 'mean', 'sum', 'var'],
        'AMT_INSTALMENT': ['max', 'mean', 'sum'],
        'AMT_PAYMENT': ['min', 'max', 'mean', 'sum'],
        'DAYS_ENTRY_PAYMENT': ['max', 'mean', 'sum']
    }
    weighted_agg = {}
    for cat in cat_cols: non_weighted_aggregations[cat] = ['mean']
    for colname in weighted_cols: weighted_agg[colname] = ['sum']
    ins_agg = ins.groupby('SK_ID_CURR').agg({**non_weighted_aggregations, **weighted_agg})
    ins_agg.columns = pd.Index(
        ['INSTAL_' + e[0] + "_" + e[1].upper() for e in ins_agg.columns.tolist()])
    # Count installments accounts
    ins_agg['INSTAL_COUNT'] = ins.groupby('SK_ID_CURR').size()
    del ins
    gc.collect()
    return ins_agg


def credit_card_balance(num_rows=None, nan_as_category=True):
    """Preprocess credit_card_balance.csv"""
    cc = pd.read_csv(os.path.join(raw_data_dir, 'credit_card_balance.csv'), nrows=num_rows)
    cc, cat_cols = one_hot_encoder(cc, nan_as_category=True)

    # Columns to apply weighted average and non-weighted aggregation metrics
    cols_to_apply_weights = ['AMT_BALANCE',
                             'AMT_CREDIT_LIMIT_ACTUAL', 'AMT_DRAWINGS_ATM_CURRENT',
                             'AMT_DRAWINGS_CURRENT', 'AMT_DRAWINGS_OTHER_CURRENT',
                             'AMT_DRAWINGS_POS_CURRENT', 'AMT_INST_MIN_REGULARITY',
                             'AMT_PAYMENT_CURRENT', 'AMT_PAYMENT_TOTAL_CURRENT',
                             'AMT_RECEIVABLE_PRINCIPAL', 'AMT_RECIVABLE', 'AMT_TOTAL_RECEIVABLE',
                             'CNT_DRAWINGS_ATM_CURRENT', 'CNT_DRAWINGS_CURRENT',
                             'CNT_DRAWINGS_OTHER_CURRENT', 'CNT_DRAWINGS_POS_CURRENT',
                             'CNT_INSTALMENT_MATURE_CUM', 'SK_DPD', 'SK_DPD_DEF',
                             'NAME_CONTRACT_STATUS_Active', 'NAME_CONTRACT_STATUS_Completed',
                             'NAME_CONTRACT_STATUS_nan']
    cols_non_weighted_metrics = cols_to_apply_weights
    cc, weighted_cols = apply_freshness_coefficient(cc, cols_to_apply_weights,
                                                    'MONTHS_BALANCE', 'SK_ID_CURR')

    # Aggregate
    non_weighted_agg = {}
    weighted_agg = {}
    for colname in cols_non_weighted_metrics: non_weighted_agg[colname] = ['min', 'max', 'mean',
                                                                           'sum', 'var']
    for colname in weighted_cols: weighted_agg[colname] = ['sum']

    cc.drop(['SK_ID_PREV'], axis=1, inplace=True)
    cc_agg = cc.groupby('SK_ID_CURR').agg({**non_weighted_agg, **weighted_agg})
    cc_agg.columns = pd.Index(
        ['CC_' + e[0] + "_" + e[1].upper() for e in cc_agg.columns.tolist()])

    # Count credit card lines
    cc_agg['CC_COUNT'] = cc.groupby('SK_ID_CURR').size()
    del cc
    gc.collect()
    return cc_agg


def main(debug=False):
    num_rows = 10000 if debug else None
    with timer("Process application"):
        df = application_train_test(num_rows)
    with timer("Process bureau and bureau_balance"):
        bureau = bureau_and_balance(num_rows)
        print("Bureau df shape:", bureau.shape)
        df = df.join(bureau, how='left', on='SK_ID_CURR')
        del bureau
        gc.collect()
    with timer("Process previous_applications"):
        prev = previous_applications(num_rows)
        print("Previous applications df shape:", prev.shape)
        df = df.join(prev, how='left', on='SK_ID_CURR')
        del prev
        gc.collect()
    with timer("Process POS-CASH balance"):
        pos = pos_cash(num_rows)
        print("Pos-cash balance df shape:", pos.shape)
        df = df.join(pos, how='left', on='SK_ID_CURR')
        del pos
        gc.collect()
    with timer("Process installments payments"):
        ins = installments_payments(num_rows)
        print("Installments payments df shape:", ins.shape)
        df = df.join(ins, how='left', on='SK_ID_CURR')
        del ins
        gc.collect()
    with timer("Process credit card balance"):
        cc = credit_card_balance(num_rows)
        print("Credit card balance df shape:", cc.shape)
        df = df.join(cc, how='left', on='SK_ID_CURR')
        del cc
        gc.collect()
    with timer("Filter non-varying features"):
        cols_to_drop = [col for col in df.columns if df[col].nunique() == 1]
        cols_to_drop.append('index')
        print('Features to be dropped :')
        print(cols_to_drop)
        df.drop(cols_to_drop, axis=1, inplace=True)
    with timer("Save dataframe to csv"):
        print(df.shape)
        df.to_csv(os.path.join(processed_data_dir, 'df.csv'), index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", default=False, type=bool)
    args = parser.parse_args()

    with timer("Preprocess data"):
        main(debug=args.debug)
