# Home Credit Default Risk

This repository host my work on the Home Credit Default Risk kaggle competition where I finished in the top 14%.

## Setup env

Create virtual env and activate it
```bash
virtualenv -p python3 venv
source venv/bin/activate
```

Install python dependencies
```bash
pip install -r requirements.txt
```

Go to the [kaggle competition webpage](https://www.kaggle.com/c/home-credit-default-risk?rvi=1) and download the competition data into `data/raw`

## Code structure
```bash
   |-data
   |---raw --> raw data from kaggle competition
   |---processed --> processed data with features
   |---feature_selection_data_dir --> processed data with only selected features
   |---submission --> predicted data from models
   |-models --> trained models
   |---lgbm_1
   |---lgbm_2
   |---meta_lr_1
   |---meta_mean
   |---mlp_1
   |---xgb_1
   |-notebooks --> exploratory notebooks
   |-src
```   

## Todo
- fix python dependencies versions and migrate to new versions
