import argparse
import os

from feature_selection import feature_selection_data_dir
from src.utils import timer, load_csv_to_df

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv_filepath", default='data/processed/df.csv')
    parser.add_argument("--feature_list_to_drop",
                        default='data/feature_selection/features_to_drop.txt')
    args = parser.parse_args()

    with open(args.feature_list_to_drop, 'r') as f:
        features_to_drop = f.readlines()
        features_to_drop = [line.strip() for line in features_to_drop]

    with timer("Load dataframe from csv"):
        df = load_csv_to_df(args.csv_filepath)

    with timer("Drop features"):
        df_filtered = df.drop(features_to_drop, axis=1)
        print('New shape:', df_filtered.shape)

    with timer("Save filtered dataframe to csv"):
        df_filtered.to_csv(os.path.join(feature_selection_data_dir, 'df.csv'), index=False)
