import argparse
import gc
import os

import numpy as np
import pandas as pd
from lightgbm import LGBMClassifier
from scipy.stats import rankdata
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import RFE, SelectFromModel
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import Imputer, MinMaxScaler

from src.utils import load_csv_to_df, timer

feature_selection_data_dir = 'data/feature_selection'


def compute_supports_corr(X, y, k=100):
    """Select feature according to correlation score"""
    df_supports = pd.DataFrame(
        X.apply(lambda feature: np.corrcoef(feature, y)[0, 1], axis=0))
    df_supports.columns = ['corr_score']
    df_supports['corr_ranking'] = rankdata(-np.abs(df_supports['corr_score'])).astype(int)
    df_supports['corr_selection'] = df_supports['corr_ranking'] < k
    return df_supports['corr_selection']


def compute_supports_chi2(X, y, k=100):
    """Select feature according to chi2 scores"""
    X_norm = pd.DataFrame(MinMaxScaler().fit_transform(X))
    X_norm.columns = X.columns
    chi2_results = chi2(X_norm, y)
    df_supports = pd.DataFrame({
        'colname': X_norm.columns,
        'chi2_score': chi2_results[0],
        'chi2_pvalue': chi2_results[1],
        'chi2_ranking': rankdata(chi2_results[1]),
    })
    df_supports.set_index('colname', inplace=True)
    df_supports['chi2_selection'] = df_supports['chi2_ranking'] < k
    return df_supports['chi2_selection']


def compute_supports_rfe_lr(X, y, k=100):
    """Select features using recursive feature elimination (RFE) with logistic regression.
    Rank depends on when feature has been eliminated. Rank == 1 means feature has has been kept."""
    X_norm = pd.DataFrame(MinMaxScaler().fit_transform(X))
    X_norm.columns = X.columns
    rfe_selector = RFE(estimator=LogisticRegression(), n_features_to_select=k, step=10, verbose=5)
    rfe_selector.fit(X_norm, y)
    df_supports = pd.DataFrame({
        'colname': X_norm.columns,
        'rfe_lr_ranking': rfe_selector.ranking_,
    })
    df_supports.set_index('colname', inplace=True)
    df_supports['rfe_lr_selection'] = df_supports['rfe_lr_ranking'] == 1
    return df_supports['rfe_lr_selection']


def compute_supports_lr(X, y, threshold=1e-5):
    """Select features using SelectFromModel with logistic regression and L1 norm"""
    X_norm = pd.DataFrame(MinMaxScaler().fit_transform(X))
    X_norm.columns = X.columns

    embeded_lr_selector = SelectFromModel(LogisticRegression(penalty="l1"), threshold=threshold)
    embeded_lr_selector.fit(X_norm, y)
    df_supports = pd.DataFrame({
        'colname': X_norm.columns,
        'lr_selection': embeded_lr_selector.get_support(),
    })
    df_supports.set_index('colname', inplace=True)
    print("{} features have been selected using Logistic Regression and threshold = {}".format(
        df_supports.lr_selection.sum(), threshold))
    return df_supports


def compute_supports_rf(X, y, threshold=1e-5):
    """Select features using SelectFromModel with RandomForest"""
    embeded_rf_selector = SelectFromModel(RandomForestClassifier(n_estimators=100),
                                          threshold=threshold)
    embeded_rf_selector.fit(X, y)
    df_supports = pd.DataFrame({
        'colname': X.columns,
        'rf_selection': embeded_rf_selector.get_support(),
    })
    df_supports.set_index('colname', inplace=True)
    print("{} features have been selected using Random Forest and threshold = {}".format(
        df_supports.rf_selection.sum(), threshold))
    return df_supports


def compute_supports_lgbm(X, y, threshold=1e-5):
    """Select features using SelectFromModel with LightGBM"""
    lgbc = LGBMClassifier(n_estimators=500, learning_rate=0.05, num_leaves=32, colsample_bytree=0.2,
                          reg_alpha=3, reg_lambda=1, min_split_gain=0.01, min_child_weight=40)
    embeded_lgb_selector = SelectFromModel(lgbc, threshold=threshold)
    embeded_lgb_selector.fit(X, y)
    df_supports = pd.DataFrame({
        'colname': X.columns,
        'lgbm_selection': embeded_lgb_selector.get_support(),
    })
    df_supports.set_index('colname', inplace=True)
    print("{} features have been selected using LightGBM and threshold = {}".format(
        df_supports.lgbm_selection.sum(), threshold))
    return df_supports


def compute_supports_all_methods(df_train):
    """Compute supports according to several feature selection methods:
    chi2, correlation, logistic regression with rfe, random forest, lightGBM
    https://www.kaggle.com/sz8416/6-ways-for-feature-selection/notebook"""

    X_train = df_train.drop(['TARGET', 'SK_ID_CURR'], axis=1)
    y_train = df_train['TARGET']

    print('Feature selection with correlation coefficient')
    df_supports_corr = compute_supports_corr(X_train, y_train, k=300)

    print('Feature selection with chi2 coefficient')
    df_supports_chi2 = compute_supports_chi2(X_train, y_train, k=300)

    print('Feature selection with recursive feature elimination and Logistic Regression')
    df_supports_rfe_lr = compute_supports_rfe_lr(X_train, y_train, k=300)

    print('Feature selection with Logistic Regression and SelectionFromModel')
    df_supports_lr = compute_supports_lr(X_train, y_train, threshold='median')

    print('Feature selection with Random Forest and SelectionFromModel')
    df_supports_rf = compute_supports_rf(X_train, y_train, threshold='median')

    print('Feature selection with Light GBM and SelectionFromModel')
    df_supports_lgbm = compute_supports_lgbm(X_train, y_train, threshold='median')

    #df_supports_list = [df_supports_corr, df_supports_chi2,
    df_supports_list = [df_supports_corr, df_supports_chi2, df_supports_rfe_lr,
                        df_supports_lr, df_supports_rf, df_supports_lgbm]

    # Check that all index are equal then concatenate dataframes
    assert all([df.index == X_train.columns] for df in df_supports_list)
    df_supports_all_methods = pd.concat(df_supports_list, axis=1)
    return df_supports_all_methods


def select_features_to_drop(df_supports, n_min_cols_to_drop=300):
    df_supports['keep_sum'] = df_supports.sum(axis=1)
    selection_criteria = select_criteria_to_filter_columns(df_supports, n_min_cols_to_drop)
    df_supports['feature_to_drop'] = df_supports['keep_sum'] < selection_criteria

    print("{} features are to be dropped with criteria {}".format(
        df_supports['feature_to_drop'].sum(),
        selection_criteria))
    return df_supports[df_supports['feature_to_drop']].index.tolist()


def select_criteria_to_filter_columns(df_supports, n_min_cols_to_drop):
    for criteria in range(max(df_supports['keep_sum'])):
        n_cols_to_drop_with_criteria = sum(df_supports['keep_sum'] < criteria)
        if n_cols_to_drop_with_criteria > n_min_cols_to_drop:
            return criteria


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv_filepath", default='data/processed/df.csv')
    parser.add_argument("--debug", default=False, type=bool)
    args = parser.parse_args()

    num_rows = 1500 if args.debug else 150000
    with timer("Load dataframe from csv"):
        df = load_csv_to_df(args.csv_filepath, num_rows)
        df_train = df[df['TARGET'].notnull()]

    with timer("Drop columns with only nan values"):
        df_train_clean = df_train.dropna(axis=1, how='all')
        features_to_drop = list(set(df_train.columns) - set(df_train_clean.columns))
        if len(features_to_drop) > 0:
            print("Dropped {} columns with only nans".format(len(features_to_drop)))
            print(features_to_drop)
        gc.collect()

    # Replace missing values
    with timer("Replace missing values"):
        fill_NaN = Imputer(missing_values=np.nan, strategy='median')
        df_train_imputed = pd.DataFrame(fill_NaN.fit_transform(df_train_clean))
        df_train_imputed.columns = df_train.columns
        df_train_imputed.index = df_train.index

    # Select features to drop
    with timer("Select features to drop"):
        df_supports_all_methods = compute_supports_all_methods(df_train_imputed)
        features_to_drop += select_features_to_drop(df_supports_all_methods, n_min_cols_to_drop=400)

    # Save column list to drop
    with open(os.path.join(feature_selection_data_dir, 'features_to_drop.txt'), 'w') as f:
        f.write('\n'.join(features_to_drop))
    df_supports_all_methods.to_csv(
        os.path.join(feature_selection_data_dir, 'supports_all_methods.csv'))
